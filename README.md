# RiskTools Global API

## API Basics

### Requests
All requests use HTTP method POST.

**Url structure:** htts://[subdomain].risktools.pro/[api_method]

#### Headers:

Content-type: application/json

AuthKey: *your_auth_key*

#### Body:
json data

### Responses

#### Success example:
```json
{
  "status": "ok",
  "result": {
    "foo": 123
  },
  "exec_time_ms": 6.635,
  "request_id": "c65ff2b8-d5ee-47da-a4f4-d6272706170d"
}
```
The result data is placed in the **result** field of the response.

#### Error example
```json
{
  "status": "error",
  "error": {
    "message": "Validation errors",
    "code": 400,
    "origin": "global",
    "errors": {
      "foo": [
        "Not a valid integer."
      ]
    }
  },
  "request_id": "d2ff0267-8a91-4c9e-b38c-b04dab697a7d"
}
```
## API Methods

### Echo
Returns the data you send. You can use this method for testing API integration

Method: **/system/echo**

Data: any data

### Process application
The main application processing API method.

Method: **/apps/process**

#### Example request data:
```json
{
  "id": "a123",
  "applied_at": "2023-01-25T12:00:00+02",
  "app_amount": 12345.0,
  "loan_term": 10,
  "term_units": "D",
  "interest_rate": 0.02,
  "interest_rate_units": "D",
  "ext_data": {
    "ip": "127.0.0.1"
  },
  "borrower": {
    "id": "u123",
    "tax_id": "1234567890",
    "full_name": "John Doe",
    "last_name": "Doe",
    "first_name": "John",
    "birth_date": "1990-01-02",
    "phone": "2349071233412",
    "email": "johndoe@example.com",
    "ext_data": {
      "address": "somewhere"
    }
  },
  "datasources": [
    {
      "provider": "Mono",
      "data_type": "account",
      "data": {
        "meta": {
          "data_status": "AVAILABLE",
          "auth_method": "mobile_banking"
        },
        "account": {
          "_id": "5feec8ce95e8dc6a52e53257",
          "institution": {
            "name": "GTBank",
            "bankCode": "058",
            "type": "PERSONAL_BANKING"
          },
          "name": "HASSAN ABDULHAMID TOMIWA",
          "accountNumber": "0131863463",
          "type": "SAVINGS ACCOUNT",
          "balance": 538786,
          "currency": "NGN",
          "bvn": "1595"
        }
      }
    },
    {
      "provider": "Mono",
      "data_type": "account",
      "data": {
        "meta": {
          "count": 190,
          "requested_length": 90,
          "available_length": 361
        },
        "data": [
          {
            "_id": "5f171a540295e231abca1154",
            "amount": 10000,
            "date": "2020-07-21T00:00:00.000Z",
            "narration": "TRANSFER from HASSAN ABDULHAMID TOMIWA to OGUNGBEFUN OLADUNNI KHADIJAH",
            "type": "credit",
            "category": "E-CHANNELS"
          },
          {
            "_id": "5e171a540295e231abca3377",
            "amount": 20000,
            "date": "2020-07-21T00:00:00.000Z",
            "narration": "TRANSFER from HASSAN ABDULHAMID TOMIWA to UMAR ABDULLAHI",
            "type": "debit",
            "category": "E-CHANNELS"
          }
        ]
      }
    }
  ]
}
```
#### Request fields description
Most of the field names are self-descriptive but some of them require explanation.

**term_units**: units of loan term. Allowed values are **"D"** (day), **"M"** (month).

**interest_rate_units**: units of interest rate. Allowed values are **"D"** (day), **"M"** (month), **"Y"** (year).

**ext_data**: any application fields collected on client side.

**borrower.ext_data**: any extended borrower data

**datasources**: this block contains the **list** of the raw data blocks requested from external datasources by the client side (CRM, mobile application, website etc).

**datasources.provider**: the name of the datas provider

**datasources.data_type**: the data provider can provide multiple types of data (types of requests). This field contains the name of data type (request type, api endpoint, etc)

**datasources.data**: the raw data received from data provider

#### Example response data
```json
{
  "status": "ok",
  "result": {
    "decision": {
      "amount_limit_max": 3000.0
    },
    "insights": {
      "score": 0.567
    },
    "datasources": [
      {
        "provider": "periculum",
        "data_type": "custom_bureau_data",
        "data": {
          "statementKey": 1,
          "bvn": "******",
          "mfCreditHasCreditFacilities": "YES",
          "mfCreditNumberOfDelinqCreditFacilities": "0",
          "mgCreditHasCreditFacilities": "NO",
          "mgCreditNumberOfDelinqCreditFacilities": "0",
          "creditHasCreditFacilities": "YES",
          "creditNumberOfDelinqCreditFacilities": "0",
          "creditLastReportedDate": "28-OCT-2021",
          "citizenship": "NG",
          "dateOfBirth": "15-JAN-1993",
          "firstName": "John",
          "lastName": "Doe",
          "gender": "001",
          "mailTo": "johndoe@company.com",
          "productName": "Insights",
          "reportDate": "05-Nov-2021",
          "providerSource": "Custom",
          "statementBureauAccountData": [
            {
              "accountName": "John Doe",
              "accountNo": "0198********53",
              "balance": "52000",
              "type": "Open",
              "payStatus": "paid",
              "paymentTerms": "monthly",
              "dateOpened": "1/1/2020",
              "responsibility": "none",
              "addressIdentificationNumber": "A-25",
              "creditLimit": "500000",
              "highBalance": "300000",
              "monthlyPayment": "10000",
              "recentPaymentAmount": "20000",
              "statementBureauAccountPaymentHistory": [
                {
                  "month": "July",
                  "year": "2022",
                  "status": "OK"
                },
                {
                  "month": "June",
                  "year": "2022",
                  "status": "OK"
                }
              ],
              "statementBureauAccountAddressHistory": [
                {
                  "address": "L-23",
                  "addressType": "Residential",
                  "dateReported": "1/JAN/2020"
                },
                {
                  "address": "L-24",
                  "addressType": "Residential",
                  "dateReported": "1/FEB/2020"
                }
              ],
              "statementBureauAccountContactHistory": [
                {
                  "contactType": "3484600",
                  "details": "dummy details",
                  "dateReported": "1/1/2020"
                },
                {
                  "contactType": "2493268",
                  "details": "dummy details 2",
                  "dateReported": "1/1/2020"
                }
              ],
              "statementBureauAccountHeaderReasons": [
                {
                  "reason": "Reason1"
                },
                {
                  "reason": "Reason2"
                }
              ]
            },
            {
              "accountName": "John",
              "accountNo": "5160********53",
              "balance": "24000",
              "type": "Open",
              "payStatus": "unpaid",
              "paymentTerms": "bi-weekly",
              "dateOpened": "1/5/2020",
              "responsibility": "none",
              "addressIdentificationNumber": "K9",
              "creditLimit": "100000",
              "highBalance": "20000",
              "monthlyPayment": "5000",
              "recentPaymentAmount": "10000",
              "statementBureauAccountPaymentHistory": [
                {
                  "month": "March",
                  "year": "2022",
                  "status": "OK"
                },
                {
                  "month": "April",
                  "year": "2022",
                  "status": "OK"
                }
              ],
              "statementBureauAccountAddressHistory": [
                {
                  "address": "L-25",
                  "addressType": "Residential",
                  "dateReported": "1/MAR/2020"
                },
                {
                  "address": "L-26",
                  "addressType": "Residential",
                  "dateReported": "1/APR/2020"
                }
              ],
              "statementBureauAccountContactHistory": [
                {
                  "contactType": "3358066",
                  "details": "dummy details1 second account",
                  "dateReported": "1/1/2020"
                },
                {
                  "contactType": "2327048",
                  "details": "dummy details2 second account",
                  "dateReported": "1/1/2020"
                }
              ],
              "statementBureauAccountHeaderReasons": [
                {
                  "reason": "Reason1 second account"
                },
                {
                  "reason": "Reason2 second account"
                }
              ]
            }
          ]
        }
      },
      {
        "provider": "periculum",
        "data_type": "blacklist",
        "data": {
          "statementKey": 2145,
          "bvn": "******",
          "mfCreditHasCreditFacilities": "YES",
          "mfCreditNumberOfDelinqCreditFacilities": "3",
          "mgcreditHasCreditFacilities": "NO",
          "mgCreditNumberOfDelinqCreditFacilities": "0",
          "creditHasCreditFacilities": "YES",
          "creditNumberOfDelinqCreditFacilities": "1",
          "creditLastReportedDate": "31-DEC-2022",
          "citizenship": "NG",
          "dateOfBirth": "19-FEB-1983",
          "firstName": "KWASI",
          "lastName": "ADAMU",
          "gender": "001",
          "mailTo": "support@crccreditbureau.com",
          "productName": "Nano Consumer Report",
          "reportDate": "02/Feb/2023",
          "providerSource": "Blacklist"
        }
      }
    ]
  },
  "exec_time_ms": 14.002,
  "request_id": "3e2320b4-ec4f-4920-84d9-f51416e39724"
}
```
#### Response fields description
**decision**: this block contains fields and flags of application decision.

**decision.amount_limit_max**: the max amount allowed for the application. Zero means reject.

**insights**: contains metrics of the application risk analysis (credit scores and other analytics data).

**datasources**: the same structure as in the request, but in response this list contains data blocks requested by RiskTools during application processing.

### Update application (loan) statuses


Method: **/apps/upsert**

#### Example request data:
```json
{
  "wait": false,
  "apps": [
    {
      "id": "12345",
      "status": {
        "loan_amount": 15000.0,
        "status": "closed",
        "issued_at": "2023-07-10T11:36:05.092735+01:00",
        "closed_at": "2023-08-09T11:36:05.092993+01:00",
        "last_paid_at": "2023-08-09T11:33:05.093048+01:00",
        "status_updated_at": "2023-08-09T11:36:05.093124+01:00",
        "total_paid": 15500.0,
        "max_overdue_days": 3,
        "max_overdue_amount": 12000.0,
        "current_overdue_days": 0,
        "current_overdue_amount": 0.0
      }
    },
    {
      "id": "12346",
      "status": {
        "loan_amount": 20000.0,
        "status": "issued",
        "issued_at": "2023-07-25T11:36:05.094966+01:00",
        "status_updated_at": "2023-07-25T11:36:05.095029+01:00"
      }
    },
    {
      "id": "12347",
      "status": {
        "loan_amount": 18000.0,
        "status": "overdue",
        "issued_at": "2023-07-10T11:36:05.095677+01:00",
        "last_paid_at": "2023-07-25T11:36:05.095736+01:00",
        "status_updated_at": "2023-08-09T11:36:05.095793+01:00",
        "total_paid": 5000.0,
        "max_overdue_days": 10,
        "max_overdue_amount": 18000.0,
        "current_overdue_days": 10,
        "current_overdue_amount": 18000.0
      }
    },
    {
      "id": "12348",
      "status": {
        "loan_amount": 18000.0,
        "status": "restructured",
        "issued_at": "2022-10-13T11:36:05.096497+01:00",
        "last_paid_at": "2023-07-25T11:36:05.096552+01:00",
        "status_updated_at": "2023-08-09T00:00:00+01:00",
        "total_paid": 12000.0,
        "max_overdue_days": 180,
        "max_overdue_amount": 18000.0,
        "current_overdue_days": 3,
        "current_overdue_amount": 4000.0
      }
    },
    {
      "id": "12349",
      "status": {
        "loan_amount": 15000.0,
        "status": "longated",
        "issued_at": "2023-07-10T11:36:05.097319+01:00",
        "closed_at": null,
        "last_paid_at": "2023-08-09T11:16:05.097370+01:00",
        "status_updated_at": "2023-08-09T11:36:05.097426+01:00",
        "total_paid": 5000.0,
        "max_overdue_days": 0,
        "max_overdue_amount": 0.0,
        "current_overdue_days": 0,
        "current_overdue_amount": 0.0
      }
    }
  ]
}
```

#### Request fields description
**wait** (boolean): If true the server will wait for all applications to be updated and return a dictionary of validation errors. Otherwise, the update will be performed asynchronously and the response will be returned immediately without information about validation. The estimated update time for 1k applications is 5 seconds in the mode of "wait": true.

**apps** (list): List of applications to update

##### Application item fields
**id** (string, required): Application ID. If the application (loan) was not processed by decision engine (RiskTools), then other application fields, described in /apps/process API method (such as borrower or term_units) should be added to the update item

**status** (dictionary): Collection of loan status fields described below.

##### Status fields description (inside status root field of each update item)
**loan_amount** (float): Actual issued loan amount

**status** (string): Loan status keyword. Allowed statuses are:
- new - application before sent to decision engine
- rejected - rejected application
- approved - approved application before payment processing
- issued - issued loan with processed outgoing payment
- closed - closed loan
- overdue - overdue loan
- longated - extended (longated) loan
- restructured - loan is restructed from overdue state
- canceled - application is cancelled by user after loan decision

**issued_at** (datetime): The timestamp of the event when the loan marked as "issued" in the billing system.

**closed_at** (datetime): The timestamp of the event when the loan marked as "closed" in the billing system.

**last_paid_at** (datetime): The timestamp of the last *incoming* payment (from borrower to company).

**status_updated_at** (datetime): The timestamp of the last billing update. This fields allows to track real changes in cases when statuses are resent for old loans.

**total_paid** (float): The amount of all customer payments on the loanю

**max_overdue_days** (integer): The highest value of overdue days maid by the borrower on the loan

**max_overdue_amount** (float): The highest overdue amount maid by the borrower on the loan

**current_overdue_days** (integer): Current overdue days

**current_overdue_amount** (float): Current overdue amount

All fields that have an undefined value, according to the logic of the credit status (for example, closed_at for open loans), can be omitted or passed *null*


#### Response description

The response is a dictionary of the following structure:

keys are application ids

values are dictionaries of error descriptions

If wait is false then response dictionary is empty


### Update payment statuses

The idea behind payment updates is almost similar to application updates.

**NOTE!** Only success payments must be sent!

Method: **/payments/upsert**

#### Example request data:
```json
{
  "wait": false,
  "payments": [
    {
      "id": "13784856",
      "application_id": "10000016",
      "borrower_id": "10000004",
      "amount": -1000.0,
      "direct_debit": 0,
      "paid_at": "2023-03-23T14:08:15+02:00"
    },
    {
      "id": "13784858",
      "application_id": "10000016",
      "borrower_id": "10000004",
      "amount": 200.0,
      "direct_debit": 0,
      "paid_at": "2023-03-23T16:01:35+02:00"
    },
    {
      "id": "13784859",
      "application_id": "10000017",
      "borrower_id": "10000005",
      "amount": 250.0,
      "direct_debit": 1,
      "paid_at": "2023-03-23T16:07:29+02:00"
    }
  ]

}
```

#### Request fields description
**wait** (boolean): If true the server will wait for all applications to be updated and return a dictionary of validation errors. Otherwise, the update will be performed asynchronously and the response will be returned immediately without information about validation. The estimated update time for 1k applications is 5 seconds in the mode of "wait": true.

**payments** (list): List of payments to update

##### Payment item fields
**id** (string, required): Application ID. If the application (loan) was not processed by decision engine (RiskTools), then other application fields, described in /apps/process API method (such as borrower or term_units) should be added to the update item

**application_id** (string)

**borrower_id** (string)

**amount** (float): The payment amount. For outgoing (to borrower) payments MUST be **negative**.

**direct_debit** (boolean): Flag of direct debit (recurring) payment

**paid_at** (string): Payment timestamp



#### Response description

The response is a dictionary of the following structure:

keys are application ids

values are dictionaries of error descriptions

If wait is false then response dictionary is empty
